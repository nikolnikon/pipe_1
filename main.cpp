#include <unistd.h>
#include <vector>
#include <utility>
#include <string>
#include <iostream>
#include <list>

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define COMMAND "who | sort | uniq -c | sort -nk1"

using namespace std;

void conv(int fd[2], list<string>item_list)
{
    if (item_list.size() < 2) {
        fd[1] = open("/home/box/result.out", O_RDWR | O_CREAT, 0666);
        
        // тут закрываем ненужные дескрипторы и dup2
        close(STDOUT_FILENO);
        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);
        close(fd[0]);
        
        string item = item_list.front();
        item_list.pop_front();

        size_t pos = item.find_first_of(" ");
        string prog = item.substr(0, pos);
        string arg;
        if (pos == string::npos)
            arg = "";
        else
            arg = item.substr(pos + 1);

        cerr << "prog: " << prog << endl;
        cerr << "arg: " << arg << endl;
        
        if (! arg.empty())
            cout << execlp(prog.c_str(), prog.c_str(), arg.c_str(), NULL) << endl;
        else
            cout << execlp(prog.c_str(), prog.c_str(), NULL) << endl;
        
        return;
    }

    pipe(fd); 

    if (!fork()) {

        // тут закрываем ненужные дескрипторы и dup2
        close(STDOUT_FILENO);
        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);
        close(fd[0]);

        string item = item_list.front();
        item_list.pop_front();

        size_t pos = item.find_first_of(" ");
        string prog = item.substr(0, pos);
        string arg;
        if (pos == string::npos)
            arg = "";
        else
            arg = item.substr(pos + 1);

        cerr << "prog: " << prog << endl;
        cerr << "arg: " << arg << endl;

        if (! arg.empty())
            cout << execlp(prog.c_str(), prog.c_str(), arg.c_str(), NULL) << endl;
        else
            cout << execlp(prog.c_str(), prog.c_str(), NULL) << endl;
    }
    else { 
        // тут закрываем ненужные дескрипторы и dup2

        close(STDIN_FILENO);
        dup2(fd[0], STDIN_FILENO);
        close(fd[1]);
        close(fd[0]);

        int pfd[2];

        item_list.pop_front();
        conv(pfd, item_list);
    }

}

int main(int argc, char **argv)
{
    string input;
    
    getline(cin, input);

    //cout << "input: " << input << endl;

    list<string> input_list;
    size_t pos = 0;
    size_t new_pos = 0;
    while (new_pos != string::npos) {
        new_pos = input.find(" | ", pos);
        input_list.push_back(input.substr(pos, new_pos - pos));
        pos = new_pos + strlen(" | ");
    }

    /*for (list<string>::iterator it = input_list.begin(); it != input_list.end(); ++it) {
        cout << *it << endl;
    }*/

    int fd[2];
    conv(fd, input_list);

 return 0;
}

